// bt1
function timChanLe() {
    var soChan = "";
    var soLe = "";
    for (var i = 0; i < 100; i++) {
        if (i % 2 == 0) soChan += " " + i;
        else soLe += " " + i;
    }
    document.getElementById(
        "result"
    ).innerHTML = `<p>👉Số chẵn: ${soChan}</p> <p>👉Số lẻ: ${soLe}</p>`;
}
// bt2
function demSoChiaHetCho3() {
    var count = 0;
    for (var i = 0; i < 1000; i++) {
        if (i % 3 == 0) count++;
    }
    document.getElementById(
        "result2"
    ).innerHTML = `👉Số chia hết cho 3 nhỏ hơn 1000: ${count} số`;
}
// bt3
function timSoNguyenDuongNhoNhat() {
    var sum = 0;
    var number = 0;
    while (sum < 10000) {
        number++;
        sum += number;
    }
    document.getElementById(
        "result3"
    ).innerHTML = `👉Số nguyên dương nhỏ nhất: ${number}`;
}
// bt4
function tinhTong() {
    var sum = 0;
    var soX = document.getElementById("so_x").value * 1;
    var soN = document.getElementById("so_n").value * 1;
    for (var i = 1; i <= soN; i++) {
        sum += soX ** i;
    }
    document.getElementById("result4").innerHTML = `👉Tổng: ${sum}`;
}
// bt5
function tinhGiaiThua() {
    var soN = document.getElementById("so_n5").value * 1;
    var result = 1;
    for (var i = 1; i <= soN; i++) {
        result = result * i;
    }
    document.getElementById("result5").innerHTML = `👉Giai thừa: ${result}`;
}
// bt6
function taoTheDiv() {
    var content = "";
    for (var i = 1; i <= 10; i++) {
        if (i % 2 == 0) content += `<div class="div-chan p-2">Div chẵn</div>`;
        else content += `<div class="div-le p-2">Div lẻ</div>`;
    }
    document.querySelectorAll("div-chan").style;
    document.getElementById("result6").innerHTML = `👉 ${content}`;
}
// bt7
function soNguyenTo(number) {
    count = 0;
    for (var i = 2; i <= Math.sqrt(number); i++) {
        if (number % i == 0) count++;
    }
    if (count == 0) return true;
    return false;
}
function timSoNguyenTo() {
    var result = "";

    var number = document.getElementById("number7").value * 1;
    for (var i = 2; i <= number; i++) {
        if (soNguyenTo(i) == true) result += " " + i;
    }
    document.getElementById("result7").innerHTML = `👉 ${result}`;
}
